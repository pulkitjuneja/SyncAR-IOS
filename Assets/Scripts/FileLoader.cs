﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleFileBrowser.Scripts.GracesGames;

public class FileLoader : MonoBehaviour {

	public GameObject fileManagerPrefab;
	public string fileExtension;
	void Start () {
		this.GetComponent<Button>().onClick.AddListener(initializeFileManger);
	}
	public void initializeFileManger() {
		GameObject fileManager = Instantiate(fileManagerPrefab,transform);
		fileManager.name = "FileBrowser";
		FileBrowser fileBrowserScript = fileManager.GetComponent<FileBrowser>();
		fileBrowserScript.SetupFileBrowser(Input.deviceOrientation == DeviceOrientation.Portrait ? ViewMode.Portrait : ViewMode.Landscape);
		fileBrowserScript.OpenFilePanel(this,"loadModelFromPath",fileExtension);
	}

	void loadModelFromPath(string path) {
		Debug.Log("syncar loader" + " " + path);
		GameObject model = OBJLoader.LoadOBJFile(path);
	}
}
