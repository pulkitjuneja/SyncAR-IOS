﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureHandler : MonoBehaviour {

	bool twoFingerActive = false;

	void Awake(){
		SimpleGesture.WhilePanning(handleRotation);
		SimpleGesture.While1FingerPanning(handleMovememnt);
	}

	void handleRotation(GestureInfoPan panInfo) {
		transform.RotateAround(Vector3.up,-panInfo.deltaDirection.x*Time.deltaTime);
		transform.RotateAround(Vector3.right,panInfo.deltaDirection.y*Time.deltaTime);
	}

	void handleMovememnt(GestureInfoPan panInfo) {
		if(Input.touchCount>1) return;
		transform.Translate(0,0,panInfo.deltaDirection.y*Time.deltaTime,null);
	}
}
